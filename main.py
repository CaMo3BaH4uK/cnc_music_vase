import pydub 
import math
import numpy as np
from stl import mesh

""" CONFIGURATION """
target_vase_height = 250 # Высота детали
music_filename = "Shostakovich-Simfonia-5-Iv.-Allegro-Non-Troppo.mp3" # Файл для чтения 
music_scale_factor = 50 # Амплитуда музыки
vertical_scale_factor = 10 # Множитель по вертикале
frame_scale_factor = 25000 # Пропуск кадров
limit_angle_degrees = 40 # Максимальный угол перехода
""" END OF CONFIGURATION """

vase_height = round(target_vase_height / vertical_scale_factor)
limit_angle = math.radians(limit_angle_degrees)
limit_ratio = math.tan(limit_angle)

vertices = np.array([[0,0,0]])[1:]
faces = np.array([[0,0,0]])[1:]
def read(f, normalized=False):
    """MP3 to numpy array"""
    a = pydub.AudioSegment.from_mp3(f)
    y = np.array(a.get_array_of_samples())
    if a.channels == 2:
        y = y.reshape((-1, 2))
    if normalized:
        music_array = np.float32(y)[::frame_scale_factor] / 2**15
        return a.frame_rate, music_array, len(music_array)
    else:
        return a.frame_rate, y
frame_rate, music, music_len = read(music_filename, True)
frames_per_layer = np.floor(music_len/vase_height).astype(int)
radians_per_frame = (2*np.pi)/frames_per_layer
for z in range(0,vase_height):
    for j in range(0,frames_per_layer):
        frame_number = z*frames_per_layer + j
        amp = abs(music[frame_number][0]) * music_scale_factor
        layer_z = z*vertical_scale_factor
        if z: 
            if j + 1 == frames_per_layer:
                point_1 = np.array([[(frame_number) - frames_per_layer, (frame_number) - frames_per_layer + 1, (frame_number) + 1 - (2 * frames_per_layer)]])
                point_2 = np.array([[(frame_number), (frame_number) - frames_per_layer, (frame_number) - frames_per_layer + 1 ]])
                faces = np.concatenate((faces, point_1))
                faces = np.concatenate((faces, point_2))
            else:
                point_1 = np.array([[(frame_number), (frame_number) - frames_per_layer, (frame_number) + 1 ]])
                point_2 = np.array([[(frame_number) + 1, (frame_number) - frames_per_layer, (frame_number) - frames_per_layer + 1 ]])
                faces = np.concatenate((faces, point_1))
                faces = np.concatenate((faces, point_2))
            last_amp = abs(music[frame_number-frames_per_layer][0]) * music_scale_factor
            delta_amp = amp - last_amp
            if (abs(delta_amp) / vertical_scale_factor) > limit_ratio:
                if delta_amp > 0:
                    amp = last_amp + (vertical_scale_factor * limit_ratio) 
                    music[frame_number][0] = amp / music_scale_factor
                elif delta_amp < 0:
                    amp = last_amp - (vertical_scale_factor * limit_ratio)
                    music[frame_number][0] = amp / music_scale_factor
        R = 30 + amp
        x = R * np.cos(j * radians_per_frame) * -1
        y = R * np.sin(j * radians_per_frame)
        xyz = np.array([[x, y, layer_z]])
        vertices = np.concatenate((vertices, xyz))
    print('Completed ' + str(z+1) + ' of ' + str(vase_height))
model = mesh.Mesh(np.zeros(faces.shape[0], dtype=mesh.Mesh.dtype))
for i, f in enumerate(faces):
    for j in range(3):
        model.vectors[i][j] = vertices[f[j],:]
model.save('out.stl')